# Containerized Tuning System for Database Configurations

Note: These current instructions are for a single node on your local machine. Setup on a cloud instance would need additional security-centric configuration.

## Prerequisites

- [Docker](https://www.docker.com/) installed on the host machine

## Running a parameter study:

1. Run the command `docker-compose up --build`
2. Navigate to http://localhost:8888
3. Enter `QuickStart` into the Token field. Optionally also configure a password near the bottom of the same page
4. Duplicate the template.ipynb file
5. Fill out the second cell in the newly created file
6. Run the notebook.

## Configuring a Kibana Visualization

1. Navigate to http://localhost:5601
2. In the left sidebar click "Management"
    1. Click "Index patterns"
    2. Click "Crate Index Pattern"
    3. Enter `study_`NAME_OF_YOUR_STUDY`*`
    4. Select a value for the Time Filter field name
    5. Click "Create Index Pattern"
3. In the left sidebar click on the "Visualize" tab
    1. Click "Create Visualization"

### Configuring the heatmap visualization

1. Click "Heat Map"
2. Select your created index
3. Expand the Metrics tab
4. For the "Aggregation" field, enter your prefered aggregation (`average -> Overall Throughput` is a good idea for YCSB) and enter your desired metric for the value field
5. Optional: Configure a workload separation
    1. Click the add button 
    2. Split chart on rows
    3. Select "Terms" for Aggregation and "workload.keyword" for field
    4. Order by Alphabetical, Descending if needed
6. Add an X axis for one of the parameters
    1. Click the plus button
    2. Click X axis
    3. Select "Histogram" if the value is continuous, "Terms" if it is discrete
        1. If Histogram, also enter an interval (around 1/10th to 1/20th of the parameter's range may be a good idea)
7. Repeat step 4 using a different parameter for the y-axis
8. In the options tab select options such as color, labels, and color ranges
9. Click "Apply Changes"
10. Make sure to save the visualization
    - It will be stored in an ElasticSearch volume, so visualizations will persist in Kibana on restart.
