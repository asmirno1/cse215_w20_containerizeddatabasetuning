from typing import Dict, List, Union

import docker
import subprocess
import os
import warnings
import tempfile
from distutils import dir_util
import glob  # https://stackoverflow.com/a/40755802
import string  # https://docs.python.org/3.6/library/string.html#template-strings
import subprocess

import re
import json
import yaml
import tarfile
import uuid

import optuna
import functools
import time
from datetime import datetime
import enum

import elasticsearch

BASE_IMAGE = "busybox"

DATABASE_STRING = "sqlite:///aggregate.db"

DATABASE_INIT_DELAY = 20

# TODO: Add context manager to remove created containers: https://book.pythontips.com/en/latest/context_managers.html#implementing-a-context-manager-as-a-class
class DockerLauncher:

    def __init__(self):
        self._client = docker.from_env()
        self._temporary_network = self._client.networks.create(str(uuid.uuid4()))  # TODO: Add context manager to automatically remove this

    def cleanup(self):
        for container in self._temporary_network.containers:
            self._temporary_network.disconnect(container)
        self._temporary_network.remove()

    def run_container(self: 'DockerLauncher', image_name: str, command: str = None, mounts: Dict[str, str] = {}, network_aliases=[], **kwargs) -> Union[docker.models.containers.Container, str]:
        """
        Returns the container if running in detached mode. Returns the result of the provided command otherwise.
        """
        container = self._client.containers.create(
            image_name,
            command=command,
            mounts=self._to_volume_mount_list(mounts),
            **kwargs
        )
        self._temporary_network.connect(container, aliases=network_aliases)
        container.start()
        return container

    def read_files_in_image(self: 'DockerLauncher', image: str, *filepaths) -> Dict[str, str]:
        container = self._client.containers.create(image)
        ret = {fp: self.read_file(container, fp) for fp in filepaths}
        container.remove()
        return ret

    def read_file(self: 'DockerLauncher', container: docker.models.containers.Container, target_filepath: str) -> str:
        archive_stream = container.get_archive(target_filepath)[0]
        with tempfile.NamedTemporaryFile("wb+") as t:
            for chunk in archive_stream:
                t.write(chunk)
            t.seek(0)
            ret = tarfile.TarFile(fileobj=t).extractfile(os.path.basename(target_filepath)).read()
        return ret

    def _to_volume_mount_list(self: 'DockerLauncher', mappings: Dict[str,str]) -> List[docker.types.Mount]:
        mounts = []
        for key, value in mappings.items():
            try:  # TODO: It may be possible that a volume has the same name as an absolute path: handle somehow?
                _ = self._client.volumes.get(key)
                mount_type = "volume"
            except docker.errors.NotFound as nf:
                raise nf
            mounts.append(docker.types.Mount(value, key, type=mount_type))
        return mounts

    def remove_docker_container(self: 'DockerLauncher', container: docker.models.containers.Container):
        self._temporary_network.disconnect(container)
        try:
            container.kill()
        except docker.errors.APIError as e:
            if e.status_code != 409:
                raise e
        container.remove()

    def make_volume_with_contents(self: 'DockerLauncher', host_dir: str):
        volume = self._client.volumes.create()
        container = self.run_container(BASE_IMAGE, "tail -f /dev/null", 
            mounts={
                volume.name: "/data"
            },
            detach=True
        )
        subprocess.run("docker cp -a {0}/. {1}:{2}".format(host_dir.rstrip('/'), container.name, "/data"), shell=True)
        self.remove_docker_container(container)
        return volume

    @staticmethod
    def run_command(container: docker.models.containers.Container, command: str):
        return container.exec_run(command).output.decode("UTF-8")
        

    # https://book.pythontips.com/en/latest/context_managers.html#implementing-a-context-manager-as-a-class

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        self.cleanup()

def configuration_rewrite(filepath: str, configuration: Dict[str, str]):
    with open(filepath, "r") as f:
        contents = string.Template(f.read()).safe_substitute(configuration)
    with open(filepath, "w") as f:
        f.write(contents)

def populate_template(temp_dir: tempfile.TemporaryDirectory, config_template_dir: str, configuation: Dict[str, str] = {}) -> tempfile.TemporaryDirectory:
    dir_util.copy_tree(config_template_dir, temp_dir)
    for fpath in glob.glob("{}/*".format(temp_dir), recursive=True):  # FIXME: Change to not require a dot extension at the end of the file.
        if os.path.isfile(fpath):
            configuration_rewrite(fpath, configuation)
    return temp_dir


ACTION_KEY = "action"
CATGEORY_KEY = "category"
VALUE_KEY = "value"
def ycsb_stats_to_json(ycsb_output: str):
    json_obj = {}
    matcher = re.compile(r'^\[(?P<{0}>[^\]]+)\],\s*(?P<{1}>[^,]+),\s*(?P<{2}>[^,]+)$'.format(ACTION_KEY, CATGEORY_KEY, VALUE_KEY), re.MULTILINE)
    for item in ycsb_output.splitlines():
        match = matcher.match(item)
        if match is not None:
            match_attr = match.groupdict()
            if match_attr[ACTION_KEY] not in json_obj:
                json_obj[match_attr[ACTION_KEY]] = {}
            value = match_attr[VALUE_KEY]
            try:
                value = float(value)  # Try to convert numeric types to floats
            except ValueError:
                pass
            json_obj[match_attr[ACTION_KEY]][match_attr[CATGEORY_KEY]] = value
    return json_obj

CASSANDRA_CONFIG_FILEPATH = "/etc/cassandra/cassandra.yaml"
CASSANDRA_IMAGE_NAME = "cassandra_db"
CASSANDRA_TEMPLATE_PATH = "/config_templates/cassandra"
YCSB_TEMPLATE_PATH = "/config_templates/ycsb"

def get_cassandra_config_volume(dl: DockerLauncher, configuration: Dict[str, str]):
    with tempfile.TemporaryDirectory() as temp_cassandraconfig_dir:
        y_config = yaml.load(dl.read_files_in_image(CASSANDRA_IMAGE_NAME, CASSANDRA_CONFIG_FILEPATH)[CASSANDRA_CONFIG_FILEPATH], Loader=yaml.FullLoader)
        if "update" in configuration.keys():
            for parameter, value in configuration["update"].items():
                y_config[parameter] = yaml.load(str(value), Loader=yaml.FullLoader)
        if "delete" in configuration.keys():
            for delkey in configuration["delete"]:
                if delkey in y_config:
                    del(y_config[delkey])
        dir_util.copy_tree(CASSANDRA_TEMPLATE_PATH, temp_cassandraconfig_dir)
        with open(os.path.join(temp_cassandraconfig_dir, os.path.basename(CASSANDRA_CONFIG_FILEPATH)), "w") as cassandra_config_file:
            yaml.dump(y_config, cassandra_config_file)
        return dl.make_volume_with_contents(temp_cassandraconfig_dir)

def run_test_cassandra(dl: DockerLauncher, configuration: Dict[str, str], script: str):
    with tempfile.TemporaryDirectory() as temp_ycsb_dir_runscripts, tempfile.TemporaryDirectory() as temp_ycsb_dir_workload:
        cassandra_config_volume = get_cassandra_config_volume(dl, configuration)
        import random
        cassandra_detached = dl.run_container(CASSANDRA_IMAGE_NAME,
            mounts={
                cassandra_config_volume.name: "/etc/cassandra"
            },
            network_aliases=[
                CASSANDRA_IMAGE_NAME  # This parameter may be different, depending on the docker-compose name for the service
            ],
            detach=True
        )
        time.sleep(DATABASE_INIT_DELAY)   # FIXME: Awful workaround, can't initialize schema wihout waiting until database is up
        try:
            dl.run_command(cassandra_detached, "cqlsh -f /init_config/ycsb_init.cqlsh")
        except docker.errors.APIError as e:
            print("API Error encountered, possible configuration issue.\nLogs for container:")
            print(cassandra_detached.logs())
            time.sleep(5)
            raise e
        ycsb_config_volume = dl.make_volume_with_contents(populate_template(temp_ycsb_dir_runscripts, os.path.join(YCSB_TEMPLATE_PATH, "run/cassandra")))
        ycsb_config_volume_2 = dl.make_volume_with_contents(populate_template(temp_ycsb_dir_workload, os.path.join(YCSB_TEMPLATE_PATH, "workloads")))
        ycsb_detached = dl.run_container("ycsb_runner",
            mounts={
                ycsb_config_volume.name: "/start",
                ycsb_config_volume_2.name: "/custom/workloads"
            },
            detach=True
        )
        test_output = dl.run_command(ycsb_detached, "/start/{}".format(script))
        # print(test_output)  # useful for debugging
        dl.remove_docker_container(cassandra_detached)
        dl.remove_docker_container(ycsb_detached)
        cassandra_config_volume.remove()
        ycsb_config_volume.remove()
        ycsb_config_volume_2.remove()
        return test_output

class YcsbWorkload(enum.Enum):
    coreA = "ycsb_workload_a"
    coreB = "ycsb_workload_b"
    coreD = "ycsb_workload_d"
    coreF = "ycsb_workload_f"
    customA = "ycsb_custom_workload_a"
    customB = "ycsb_custom_workload_b"
    customD = "ycsb_custom_workload_d"
    customF = "ycsb_custom_workload_f"
    customUniformB = "ycsb_custom_workload_b_uniform"

USE_ELASTICSEARCH = True

class TargetDatabase(enum.Enum):
    CassandraDB = run_test_cassandra

class Metrics(enum.Enum):
    throughput = ["OVERALL", "Throughput(ops/sec)"]

class Runner:

    def __init__(self, target_database: TargetDatabase = TargetDatabase.CassandraDB, elasticsearch_enabled=False, num_workers=5):
        self.database_func = target_database
        self.elasticsearch_enabled = elasticsearch_enabled
        self.num_workers = num_workers

    def _run_trial(self, trial: optuna.Trial, config_creation_func, workload: YcsbWorkload, metric: List[str]):
        config = config_creation_func(trial)
        with DockerLauncher() as dl:
            stats = ycsb_stats_to_json(self.database_func(dl, config, workload.value))
            if self.elasticsearch_enabled:
                data = {
                    "trial_name": trial.study.study_name,
                    "workload": workload.value,
                    "parameters": trial.params,
                    "metrics": stats,
                    "timestamp": datetime.now()
                }
                try:
                    es = elasticsearch.Elasticsearch(hosts=["elasticsearch"])  # This is the network alias configured under docker-compose
                    es.index(index=trial.study.study_name, body=data)
                except Exception as e:
                    warnings.warn("Unable to record to ElasticSearch.\n{}".format(e))
            target = stats
            for key in metric:
                try:
                    target = target[key]
                except KeyError as e:
                    print(stats)
                    raise e
            return target

    @staticmethod
    def format_study_name(study_name, workload: YcsbWorkload):
        return "study_{0}_{1}".format(study_name, workload.value)

    def delete_study(self, study_name: str):
        try:
            optuna.delete_study(study_name, DATABASE_STRING)
        except Exception as e:
            warnings.warn("Unable to delete study {0}:\n{1}".format(study_name, e))

    def run_test(self, config_creation_func, study_name: str, workload: YcsbWorkload, direction='maximize', metric: List[str] = Metrics.throughput.value, trials=10, delete_first_if_exists=False, **optuna_study_kwargs):
        if delete_first_if_exists is True:
            self.delete_study(study_name)            
        study = optuna.create_study(study_name=study_name, storage=DATABASE_STRING, load_if_exists=True, direction=direction, **optuna_study_kwargs)
        study.optimize(functools.partial(self._run_trial, config_creation_func=config_creation_func, workload=workload, metric=metric), n_trials=trials, n_jobs=self.num_workers)
        return study


def config_keycache(trial: optuna.Trial):
    key_cache_size_in_mb = trial.suggest_int("key_cache_size_in_mb", 100, 1000)
    return {
        "update": {
            "key_cache_size_in_mb": key_cache_size_in_mb,
            "broadcast_rpc_address": "127.0.0.1"
        }
    }

def config_commitlog(trial: optuna.Trial):
    commitlog_sync_type = trial.suggest_categorical("commitlog_sync", ["batch", "periodic"])
    if commitlog_sync_type is "batch":
        config = {
            "update": {
                "commitlog_sync": commitlog_sync_type,
                "commitlog_sync_batch_window_in_ms": trial.suggest_int("batch_window_ms", 100, 1000),
                "broadcast_rpc_address": "127.0.0.1"
            },
            "delete": {
                "commitlog_sync_period_in_ms"
            }
        }
    elif commitlog_sync_type is "periodic":
        config = {
            "update": {
                "commitlog_sync": commitlog_sync_type,
                "commitlog_sync_period_in_ms": trial.suggest_int("sync_period_ms", 100, 1000),
                "broadcast_rpc_address": "127.0.0.1"
            },
            "delete": {
                "commitlog_sync_batch_window_in_ms"
            }
        }
    return config